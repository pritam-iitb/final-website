from django.shortcuts import render
from rest_framework.decorators import api_view
from .models import UserProfile
from django.contrib.auth import authenticate, login
from rest_framework.response import Response
from django.http import HttpResponse

@api_view(['POST'])
def loginUser(request):
    """
    Send login data like:
    {
        "email": "__user_email__",
        "password" : "__user_password__"
    }
    """
    if request.method == 'POST':
        email = request.data['email']
        password = request.data['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)

            return Response([{"Success": "Login Succeded"}], status=200)
        else:
            return HttpResponse([{'Error':"Authentication failed"}], status=401)
