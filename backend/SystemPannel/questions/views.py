import random
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import *
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
# from . import QuestionBank
from .models import *

# Create your views here.
# ques_id=randint(1,100) #100 questions for example (to be changed later)

@login_required
@api_view(['POST'])
def addQuestion(request):
	"""
	send json like this:
	{
		"statement": "__question__",
		"tag": "m or c or n",
		"qtag": "M or A",
		"option1" : "option1",
		"option2" : "option2",
		"option2" : "option2",
		"option2" : "option2",
		"mcqanswer": "mcqanswer",
		"numanswer": "numanswer",
		"img_url": "img_url"

	}
	"""
	if request.method == 'POST':
		if request.user.is_admin:
			statement = request.data['statement']
			tag = request.data['tag']
			exam_tag = request.data['qdata']
			if tag == 'm' or tag=='c':
				option1 = request.data['option1']
				option2 = request.data['option2']
				option3 = request.data['option3']
				option4 = request.data['option4']
				mcqanswer = request.data['mcqanswer']
				QuestionBank.objects.create(question=statement,option1=option1,option2=option2,
										option3=option3,option4=option4,mcqanswer=mcqanswer,tag=tag, exam_tag=exam_tag, img_url=img_url, numanswer=None)
				return HttpResponse(status=200)
			elif tag == 'n':
				numanswer = request.data['numanswer']
				QuestionBank.objects.create(question=statement,option1=None,option2=None,
										option3=None,option4=None,mcqanswer=None,tag=tag, exam_tag=exam_tag,  img_url=img_url, numanswer=numanswer)
			else:
				return Response([{"Error": "Invalid tag"}], status=401)
		else:
			return Response([{"Error": "you are not admin"}], status=401)

# class Questions(viewsets.ModelViewSet):
# 	queryset = QuestionBank.objects.all()
# 	serializer_class = QuestionSerializer

# 	@login_required
# 	def post(self,request,*args,**kwargs):
# 		if request.user.is_authenticated:
# 			if request.user.is_admin:
# 				statement = request.data['statement']
# 				option1 = request.data['option1']
# 				option2 = request.data['option2']
# 				option3 = request.data['option3']
# 				option4 = request.data['option4']
# 				answer = request.data['answer']
# 				tag = request.data['tag']
# 				print("hello")
# 				QuestionBank.objects.create(question=statement,option1=option1,option2=option2,
# 										option3=option3,option4=option4,mcqanswer=answer,tag=tag)
# 				return HttpResponse(status=200)
# 		else:
# 			return Response([{"Error": "you are not logged in"}], status=401)



def QuestionDisplay(request,ques_id):
	question = QuestionBank.objects.get(id=ques_id)
	question_serializer = QuestionSerializer(question,many=True)
	return JsonResponse(question_serializer.data,safe=False)


# def QuestionDisplay(request,ques_id) :
#     question = Question.objects.get(id=ques_id)
#     return render(request , tempname ,{ 'question': question }) #tempname=template_name (not specified yet)
                         
def CheckAns(request , ques_id ): 
# assuming the option input to be in forms
    if request.method == 'POST':
        Chosen_ans = request.POST[''] #get_the_chosen ans.
        return render(request , tempname , {'Chosen_ans':Chosen_ans})
        #currently returning the chosen answer which can be validated in the template , the question details are passed in the method above.