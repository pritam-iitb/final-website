from .models import *
from rest_framework import serializers


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionBank
        fields = ['question', 'option1', 'option2', 'option3','option4','mcqanswer','tag']


