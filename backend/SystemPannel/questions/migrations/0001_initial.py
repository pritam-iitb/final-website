# Generated by Django 3.0.5 on 2020-06-15 17:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='QuestionBank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=700)),
                ('option1', models.CharField(default=None, max_length=400, null=True)),
                ('option2', models.CharField(default=None, max_length=400, null=True)),
                ('option3', models.CharField(default=None, max_length=400, null=True)),
                ('option4', models.CharField(default=None, max_length=400, null=True)),
                ('img_url', models.CharField(max_length=2500, null=True)),
                ('exam_tag', models.CharField(default='D', max_length=1)),
                ('tag', models.CharField(max_length=1)),
                ('numanswer', models.CharField(default=None, max_length=20, null=True)),
                ('mcqanswer', models.CharField(default=None, max_length=400, null=True)),
            ],
        ),
    ]
