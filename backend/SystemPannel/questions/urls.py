from django.urls import include, path,re_path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
# router.register('question',views.addQuestion,'question')


urlpatterns = [
    path('question/',views.addQuestion,name='question'),
    path('', include(router.urls)),
    path('showquestion/:id',views.QuestionDisplay)
]