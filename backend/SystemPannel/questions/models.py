from django.db import models

# Create your models here.
class QuestionBank(models.Model):
    question=models.CharField(max_length=700)
    option1=models.CharField(max_length=400, null=True, default=None)
    option2=models.CharField(max_length=400, null=True, default=None)
    option3=models.CharField(max_length=400, null= True, default=None)
    option4=models.CharField(max_length=400, null=True, default=None)
    img_url = models.CharField(max_length=2500, null=True)
    exam_tag = models.CharField(max_length=1, default='D') #either M for mains or N for numerical
    tag=models.CharField(max_length=1)#either m for mcq or n for numerical or c for comprehension
    numanswer=models.CharField(max_length=20, null=True, default=None)
    mcqanswer=models.CharField(max_length=400, null=True, default=None)