
 function next() {
    if (document.getElementById("s3").checked == true ) {
        document.getElementById("s3").checked = false;
        document.getElementById("s4").checked = true;
    }
    else if (document.getElementById("s1").checked == true ) {
        document.getElementById("s1").checked = false;
        document.getElementById("s2").checked = true;
    }
    else if (document.getElementById("s2").checked == true ) {
        document.getElementById("s2").checked = false;
        document.getElementById("s3").checked = true;
    }
    else  {
        document.getElementById("s4").checked = false;
        document.getElementById("s1").checked = true;
    }

}

function previous() {
    if (document.getElementById("s3").checked == true ) {
        document.getElementById("s2").checked = true;
        document.getElementById("s3").checked = false;
    }
    else if (document.getElementById("s1").checked == true ) {
        document.getElementById("s4").checked = true;
        document.getElementById("s1").checked = false;
    }
    else if (document.getElementById("s2").checked == true ) {
        document.getElementById("s1").checked = true;
        document.getElementById("s2").checked = false;
    }
    else {
        document.getElementById("s3").checked = true;
        document.getElementById("s4").checked = false;
    }

}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = "Time "+ "Left-"+ minutes + ":" + seconds;

        if (--timer < 0) {
            timer = 0;
            // timer = duration; // uncomment this line to reset timer automatically after reaching 0
        }
    }, 1000);
}

window.onload = function () {
    var time = 10800, // your time in seconds here
        display = document.querySelector('#safeTimerDisplay');
    startTimer(time, display);
};

//JavaScript function that enables or disables a submit button depending
//on whether a checkbox has been ticked or not.
function terms_changed(termsCheckBox){
    //If the checkbox has been checked
    if(termsCheckBox.checked){
        //Set the disabled property to FALSE and enable the button.
        document.getElementById("agreebutton").disabled = false;
        document.getElementById("agreebutton").classList.add('agreebutton1');
        document.getElementById("agreebutton").classList.remove('agreebutton');
    } else{
        //Otherwise, disable the submit button.
        document.getElementById("agreebutton").disabled = true;
        document.getElementById("agreebutton").classList.add('agreebutton');
        alert('Please agree to the "Terms & Conditions"!');
    }
}


window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("scrollBar").style.width = scrolled + "%";
}

