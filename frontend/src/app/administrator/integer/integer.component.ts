import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-integer',
  templateUrl: './integer.component.html',
  styleUrls: ['./integer.component.scss']
})
export class IntegerComponent implements OnInit {

  constructor(private api:HttpService) { }

  ngOnInit() {
  }

  onSubmit(){
  	let formData = new FormData();
  	formData.append('img_url', (document.querySelector("[name=qimage]") as any).value);
  	formData.append('question',(document.querySelector("[name=question]") as any).value);
  	formData.append('numanswer',(document.querySelector("[name=answer]") as any).value);
  	formData.append('tag','n');
  	formData.append('exam_tag','M');
  	this.api.createPost(this.api.host+"/question/",formData).subscribe((val)=>{
  		console.log(val)
  	});
  }

}
