import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http.service';
@Component({
  selector: 'app-comprehension',
  templateUrl: './comprehension.component.html',
  styleUrls: ['./comprehension.component.scss']
})
export class ComprehensionComponent implements OnInit {

  constructor(private api:HttpService) { }

  ngOnInit() {
  }

  onSubmit(){
  	let formData = new FormData();
  	formData.append('img_url', (document.querySelector("[name=cimage]") as any).value);
  	formData.append('question',(document.querySelector("[name=q1]") as any).value);
  	formData.append('mcqanswer',(document.querySelector("[name=answer1]") as any).value);
  	formData.append('option1',(document.querySelector("[name=option11]") as any).value);
  	formData.append('option2',(document.querySelector("[name=option21]") as any).value);
  	formData.append('option3',(document.querySelector("[name=option31]") as any).value);
  	formData.append('option4',(document.querySelector("[name=option41]") as any).value);
  	formData.append('tag','c');
  	formData.append('exam_tag','M');
  	this.api.createPost(this.api.host+"/question/",formData).subscribe((val)=>{
  		console.log(val)
  	});

  	formData = new FormData();
  	formData.append('img_url', (document.querySelector("[name=cimage]") as any).value);
  	formData.append('question',(document.querySelector("[name=q2]") as any).value);
  	formData.append('mcqanswer',(document.querySelector("[name=answer2]") as any).value);
  	formData.append('option1',(document.querySelector("[name=option12]") as any).value);
  	formData.append('option2',(document.querySelector("[name=option22]") as any).value);
  	formData.append('option3',(document.querySelector("[name=option32]") as any).value);
  	formData.append('option4',(document.querySelector("[name=option42]") as any).value);
  	formData.append('tag','c');
  	formData.append('exam_tag','M');
  	this.api.createPost(this.api.host+"/question/",formData).subscribe((val)=>{
  		console.log(val)
  	});
  }

}
