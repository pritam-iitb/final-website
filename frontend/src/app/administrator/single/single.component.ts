import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit {

  constructor(private api:HttpService) { }

  ngOnInit() {
  }

  onSubmit(){
  	let formData = new FormData();
  	formData.append('img_url', (document.querySelector("[name=qimage]") as any).value);
  	formData.append('question',(document.querySelector("[name=question]") as any).value);
  	formData.append('mcqanswer',(document.querySelector("[name=answer]") as any).value);
  	formData.append('option1',(document.querySelector("[name=option1]") as any).value);
  	formData.append('option2',(document.querySelector("[name=option2]") as any).value);
  	formData.append('option3',(document.querySelector("[name=option3]") as any).value);
  	formData.append('option4',(document.querySelector("[name=option4]") as any).value);
  	formData.append('tag','m');
  	formData.append('exam_tag','M');
  	this.api.createPost(this.api.host+"/question/",formData).subscribe((val)=>{
  		console.log(val)
  	});
  }
}
