import { Component, OnInit } from '@angular/core';
import {UserdataService} from '../services/userdata.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private ud:UserdataService) { }
  user;
  ngOnInit(): void {
  	//user may login/logout at any time. so we need to continuously track it. 
  	setInterval(()=>this.getUser())
  }

  getUser(){
  	this.user = this.ud.getUser();  
  }

}
