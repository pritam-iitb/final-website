import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VideoComponent } from './video/video.component';
import { QuestionsComponent } from './questions/questions.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SigninComponent } from './navbar/signin/signin.component';
import { RegisterComponent } from './navbar/register/register.component';
import {HttpClientModule} from '@angular/common/http';
import { MainComponent } from './navbar/main/main.component';
import { Error404Component } from './error404/error404.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { SingleComponent } from './administrator/single/single.component';
import { MultipleComponent } from './administrator/multiple/multiple.component';
import { IntegerComponent } from './administrator/integer/integer.component';
import { ComprehensionComponent } from './administrator/comprehension/comprehension.component';
import { ErrorComponent } from './error/error.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { VideoUploadComponent } from './videos-folder/video-upload/video-upload.component';
import { MyVideosComponent } from './videos-folder/my-videos/my-videos.component';
import {MathJaxModule} from 'ngx-mathjax';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VideoComponent,
    QuestionsComponent,
    NavbarComponent,
    SigninComponent,
    RegisterComponent,
    MainComponent,
    Error404Component,
    AdministratorComponent,
    SingleComponent,
    MultipleComponent,
    IntegerComponent,
    ComprehensionComponent,
    ErrorComponent,
    InstructionsComponent,
    VideoUploadComponent,
    MyVideosComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path:'',component:HomeComponent},
      {path:'questions',component:QuestionsComponent},
      {path:'signin',component:SigninComponent},
      {path:'register',component:RegisterComponent},
      {path:'main',component:MainComponent},
      {path:'video',component:VideoComponent},
      {path:'video-upload',component:VideoUploadComponent},
      {path:'my-video',component:MyVideosComponent},
      {path:'instructions',component:InstructionsComponent},
      {path:'administrator',component:AdministratorComponent,
        children: [
          {path: 'single', component: SingleComponent},
          {path: 'multiple', component: MultipleComponent},
          {path: 'integer', component: IntegerComponent},
          {path: 'comprehension', component: ComprehensionComponent},]
      },
      {path:'**',component:ErrorComponent},
    ]),
    MathJaxModule.forRoot({
      version: '2.7.5',
      config: 'TeX-AMS_HTML',
      hostname: 'cdnjs.cloudflare.com'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
