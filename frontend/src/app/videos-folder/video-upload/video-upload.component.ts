import { Component, OnInit } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-video-upload',
  templateUrl: './video-upload.component.html',
  styleUrls: ['./video-upload.component.scss']
})
export class VideoUploadComponent implements OnInit {

  

  ngOnInit() {
  }

  title: string;
  body: string;
  videofile : File;

  constructor(private http :HttpClient){}

  onTitleChanged(event:any ){
    this.title = event.target.value;
  }

  onVideoChanged(event:any ){
    this.videofile = event.target.files[0];
  }

  onBodyChanged(event:any ){
    this.body = event.target.value;
    
  }
  showMsg: boolean = false;

  newVideo(){
      const uploadData = new FormData();
      console.log("Uploaded");
      uploadData.append('title' ,this.title );
      uploadData.append('body' ,this.body);
      uploadData.append('videofile',this.videofile ,this.videofile.name);
      this.http.post('http://127.0.0.1:8000/videos/', uploadData).subscribe(
        data => console.log(data),
        error => console.log(error)
        
      )
      this.showMsg = true;
      setTimeout( () => {
        this.showMsg = false;
      }, 1000);
  }

  



  

}
