import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-my-videos',
  templateUrl: './my-videos.component.html',
  styleUrls: ['./my-videos.component.scss'],
  providers: [ApiService]
})
export class MyVideosComponent  {


  videos = [{title: 'test'}];
  selectedVideo: { id: any; title?: string; body?: string; videofile?: string; };
  

 

  constructor(private api: ApiService ) {
    this.getVideos();
    this.selectedVideo = {id:-1,title:'',body:''}
   
  }
  getVideos = () => {
    this.api.getAllVideos().subscribe(
      data => {
        this.videos = data;
      },
      error => {
        console.log(error);
      }
    );
  }



  current_video: Video={
    videofile: "video.mp4",
    title: "title",
    upvotes: 1400,
    downvotes: 3,
    
    creator: "teacher-name",
    references:[
      "1st Reference",
      "2nd reference"
    ],
    
    comments: [
      {poster: "teacher-name",
      comment: "this is the first comment",
      replies:[{
        poster: "teacher-name",
        comment: "this is the first reply",
        replies: null

      }]
    }
    ]
   
  }



}


class Video{
  videofile: String;
  title: String;
  upvotes: number;
  downvotes: number;
  
  creator: String;
  references: String[];
  comments: Comment[];
  
}
class Comment{
  poster: String;
  comment: String;
  replies: Comment[];
}











