import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private httpClient:HttpClient) {}
  host = "http://localhost:8000";
  httpHeaders = new HttpHeaders({'Content-Type':'application/json'})
	createPost(url,post){
	  return this.httpClient.post<any>(url,post);
  }
  baseurl="http://127.0.0.1:8000";
  getAllVideos():Observable<any>{
    return this.httpClient.get(this.baseurl+'/videos' ,{headers: this.httpHeaders});
  }
}