import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';


@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
  current_video: Video={
    videofile: "video.mp4",
    title: "title",
    upvotes: 1400,
    downvotes: 3,
    upnext: [
      {title: "title",
    upvotes: 2000,
    downvotes: 34,
    videofile: null,
    creator: "teacher-name",
    references: null,
    comments: null,
    upnext: null,
    thumbnail: "assets/images/user-1.jpg"
  },
  {title: "title",
  upvotes: 2000,
  downvotes: 4,
  videofile: null,
  creator: "teacher-name",
  references: null,
  comments: null,
  upnext: null,
  thumbnail: "assets/images/user-1.jpg"
},
{title: "title",
upvotes: 2000,
downvotes: 37,
videofile: null,
creator: "teacher-name",
references: null,
comments: null,
upnext: null,
thumbnail: "assets/images/user-1.jpg"
},

{title: "title",
upvotes: 2000,
downvotes: 34,
videofile: null,
creator: "teacher-name",
references: null,
comments: null,
upnext: null,
thumbnail: "assets/images/user-1.jpg"
},
{title: "title",
upvotes: 2000,
downvotes: 34,
videofile: null,
creator: "teacher-name",
references: null,
comments: null,
upnext: null,
thumbnail: "assets/images/user-1.jpg"
},
    ],
    creator: "teacher-name",
    references:[
      "1st Reference",
      "2nd reference"
    ],
    thumbnail: "assets/images/user-1.jpg",
    comments: [
      {poster: "teacher-name",
      comment: "this is the first comment",
      replies:[{
        poster: "teacher-name",
        comment: "this is the first reply",
        replies: null

      }]
    }
    ]
   
  }



  ngOnInit(): void {
    
  }
  @ViewChild("videoPlayer", { static: false }) videoplayer: ElementRef;
  isPlay: boolean = false;
  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }
}
class Video{
  videofile: String;
  title: String;
  upvotes: number;
  downvotes: number;
  upnext: Video[];
  creator: String;
  references: String[];
  comments: Comment[];
  thumbnail: String;
}
class Comment{
  poster: String;
  comment: String;
  replies: Comment[];
}